package com.vuzix.sample.barcodefromintent;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.vuzix.sample.barcodefromintent.services.opponentHandler;

public class OpponentsActivity extends Activity {

    public static final String JSON_URL="http://app02.yqlabs.com/ABSME_EP_TEST/rest/favouritesListService?regId=76";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opponents);
    }

    public void RunClickHandler(View view) {
        Intent intent=new Intent(this, opponentHandler.class);
        intent.setData(Uri.parse(JSON_URL));
        startService(intent);

    }
}
